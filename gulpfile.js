'use strict'

const gulp = require('gulp')
const sass = require('gulp-sass')
const browserSync = require('browser-sync')
const plumber = require('gulp-plumber')
const concat = require('gulp-concat')
const uglify = require('gulp-uglify')
const autoprefixer = require('gulp-autoprefixer')
const sourcemaps = require('gulp-sourcemaps')
const minify = require('gulp-minify-css')

const scssSource = './scss/style.scss'
const scssWatchDir = ['./scss/*', './scss/partials/*']
const themeDir = './www/wp-content/themes/earthxfilm/'
const proxy = 'earthxfilm.dev'
const watchDirs = [`${themeDir}*`, `${themeDir}*/*`]

gulp.task('scss', () => {
  gulp.src(scssSource)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(themeDir))
})

gulp.task('js', () => {
  gulp.src('./js/scripts.js')
    .pipe(plumber())
    .pipe(concat('scripts.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(themeDir))
})

gulp.task('watch', () => {
  gulp.watch(scssWatchDir, ['scss'], browserSync.stream)
  gulp.watch('./js/*', ['js'], browserSync.reload)
  gulp.watch(watchDirs).on('change', browserSync.reload);
})

gulp.task('serve', ['watch', 'scss', 'js'], () => {
  browserSync.init({
    proxy: proxy,
    open: false
  })
})

gulp.task('default', ['serve']);
