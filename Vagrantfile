# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

# do not add preceding www, it will be added automatically
hostname = 'earthxfilm.dev';

http_port = "49155"
https_port = "49156"
mysql_port = "49157"

# Check for missing plugins
required_plugins = %w(vagrant-triggers vagrant-hostmanager)
plugin_installed = false
required_plugins.each do |plugin|
  unless Vagrant.has_plugin?(plugin)
    system "vagrant plugin install #{plugin}"
    plugin_installed = true
  end
end

# If new plugins installed, restart Vagrant process
if plugin_installed === true
  exec "vagrant #{ARGV.join' '}"
end

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  config.vm.box = "ubuntu/trusty64"

  config.vm.network :forwarded_port, guest: 80, host: http_port
  config.vm.network :forwarded_port, guest: 443, host: https_port
  config.vm.network :forwarded_port, guest: 3306, host: mysql_port

  config.vm.hostname = hostname

  config.hostmanager.enabled = false
  config.hostmanager.manage_host = true
  config.hostmanager.ignore_private_ip = false
  config.hostmanager.include_offline = true
  config.hostmanager.aliases = "www."+hostname

  config.vm.provision :hostmanager

  config.vm.synced_folder "./www", "/vagrant",
    id: "vagrant-root",
    owner: "vagrant",
    group: "www-data",
    mount_options: ["dmode=775,fmode=664"]

  config.vm.provision :shell, :path => "provision.sh"

  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on", "--memory", "1024" ]
  end

  config.trigger.after [:provision, :up, :reload] do
      system('echo "
        rdr pass on lo0 inet proto tcp from any to 127.0.0.1 port 80 -> 127.0.0.1 port ' + http_port + '
        rdr pass on lo0 inet proto tcp from any to 127.0.0.1 port 443 -> 127.0.0.1 port ' + https_port + '
        rdr pass on lo0 inet proto tcp from any to 127.0.0.1 port 3306 -> 127.0.0.1 port ' + mysql_port + '
        " | sudo pfctl -ef - > /dev/null 2>&1; echo "==> Fowarding Ports: 80 -> ' + http_port + ', 443 -> ' + https_port + ', 3306 -> ' +  mysql_port + ' & Enabling pf"')
  end

  config.trigger.after [:halt, :destroy] do
    system("sudo pfctl -df /etc/pf.conf > /dev/null 2>&1; echo '==> Removing Port Forwarding & Disabling pf'")
  end

end
