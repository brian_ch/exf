<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitc06bbaa7fe28112bba1e5f2706d97a2c
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'Timber\\' => 7,
        ),
        'C' => 
        array (
            'Composer\\Installers\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Timber\\' => 
        array (
            0 => __DIR__ . '/..' . '/timber/timber/lib',
        ),
        'Composer\\Installers\\' => 
        array (
            0 => __DIR__ . '/..' . '/composer/installers/src/Composer/Installers',
        ),
    );

    public static $fallbackDirsPsr4 = array (
        0 => __DIR__ . '/..' . '/asm89/twig-cache-extension/lib',
    );

    public static $prefixesPsr0 = array (
        'T' => 
        array (
            'Twig_' => 
            array (
                0 => __DIR__ . '/..' . '/twig/twig/lib',
            ),
        ),
        'R' => 
        array (
            'Routes' => 
            array (
                0 => __DIR__ . '/..' . '/upstatement/routes',
            ),
        ),
    );

    public static $classMap = array (
        'AltoRouter' => __DIR__ . '/..' . '/altorouter/altorouter/AltoRouter.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitc06bbaa7fe28112bba1e5f2706d97a2c::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitc06bbaa7fe28112bba1e5f2706d97a2c::$prefixDirsPsr4;
            $loader->fallbackDirsPsr4 = ComposerStaticInitc06bbaa7fe28112bba1e5f2706d97a2c::$fallbackDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInitc06bbaa7fe28112bba1e5f2706d97a2c::$prefixesPsr0;
            $loader->classMap = ComposerStaticInitc06bbaa7fe28112bba1e5f2706d97a2c::$classMap;

        }, null, ClassLoader::class);
    }
}
