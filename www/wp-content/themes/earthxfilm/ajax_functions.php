<?php
add_action( 'wp_ajax_add_foobar', 'prefix_ajax_add_foobar' );
add_action( 'wp_ajax_nopriv_add_foobar', 'prefix_ajax_add_foobar' );
function prefix_ajax_add_foobar() {
    $post_id = $_POST['post_id'];
    $fields = get_fields($post_id);
    echo json_encode($fields);
    wp_die();
}
