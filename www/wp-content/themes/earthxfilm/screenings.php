<?php
/* Template Name: Film Screening List */
get_header();
view_header_menu();
?>
<main>
<?php
view_film_screenings();
?>
</main>
<?php
get_footer();
?>
