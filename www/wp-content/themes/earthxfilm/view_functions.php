<?php

// Global context variables
add_filter( 'timber_context', 'add_to_context' );
function add_to_context($context) {
  $context['current_url'] = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" . strtok($_SERVER["REQUEST_URI"],'?');
  $context['current_query'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
  $context['current_page_id'] = get_the_id();
  return $context;
}

add_action( 'wp_enqueue_scripts', 'enq_elevent_script' );
function enq_elevent_script() {
  wp_enqueue_script( 'elevent', get_template_directory_uri() . '/js/elevent.js', null, null, true );
}

function view_films() {
  add_action( 'wp_enqueue_scripts', 'enq_elevent_script' );
  function enq_elevent_script() {
    wp_enqueue_script( 'elevent', get_template_directory_uri() . '/js/elevent.js', null, null, true );
  }
  $context = _get_context();
  $context['films'] = Timber::get_posts('post_type=film&numberposts=1000');
  Timber::render('views/films.twig', $context);
}

function view_dynamic_meta() {
    $context = _get_context();
    // Set defaults
    $context['default_title'] = get_bloginfo('name');
    $context['default_desc']  = get_bloginfo('description');
    $context['default_image'] = get_field('image', 'option');
    // Parse query parameters into array
    parse_str($context['current_query'], $queryParams);
    // If the film_id parameter is provided
    if (isset($queryParams['film_id'])) {
        // And it is a valid film post ID
        if (get_post_type($queryParams['film_id']) === 'film') {
            // Add the film post to the context, along with its ACF data
            $context['film'] = Timber::get_post($queryParams['film_id']);
            $context['film']->acf = get_fields($queryParams['film_id']);
        }
    } else if (isset($queryParams['share_item'])) {
        $context['item'] = get_field('items')[$queryParams['share_item']];
    }

    Timber::render('views/dynamic-meta.twig', $context);
}

function view_header_menu() {
  $context = _get_context();
  $context['nav_links'] = get_field('nav_links', 'option');
  $context['menu_text'] = get_field('prenav_text', 'option');
  $context['prenav_links'] = get_field('prenav_links', 'option');
  $context['facebook_link'] = get_field('facebook_link', 'option');
  $context['twitter_link'] = get_field('twitter_link', 'option');
  $context['instagram_link'] = get_field('instagram_link', 'option');
  $context['image'] = get_field('image', 'option');
  $context['mobile_image_icon'] = get_field('mobile_image_icon', 'option');
  $context['cart_link'] = get_field('cart_link', 'option');
  Timber::render('views/header-menu.twig', $context);
}

function view_hero() {
  $context = _get_context();
  $context['image'] = get_field('hero_image', 'option');
  $context['scroll_indicator_image'] = get_field('scroll_indicator_image', 'option');
  $context['mobile_hero_section_title'] = get_field('mobile_hero_section_title', 'option');
  $context['mobile_slides'] = array_map(
    function($card){
      $card['slide']->cf = get_fields($card['slide']->ID);
      return $card['slide'];
    },
    get_field('mobile_slides', 'option')
  );
  $context['desktop_slides'] = array_map(
    function($card){
      $card['slide']->cf = get_fields($card['slide']->ID);
      return $card['slide'];
    },
    get_field('desktop_slides', 'option')
  );
  $homepage_buttons = array(
    'screening_times' => array(
      'text' => get_field('screening_times_text', 'option'),
      'link' => get_field('screening_times_link', 'option')
    ),
    'open_events' => array(
      'text' => get_field('open_events_text', 'option'),
      'link' => get_field('open_events_link', 'option')
    ),
    'ticket_info' => array(
      'text' => get_field('ticket_info_text', 'option'),
      'link' => get_field('ticket_info_link', 'option')
    ),
  );
  $context['homepage_buttons'] = $homepage_buttons;
  Timber::render('views/hero.twig', $context);
}

function view_sub_hero() {
  $context = _get_context();
  $context['sub_hero_text'] = get_field('sub_hero_text', 'option');
  Timber::render('views/sub-hero.twig', $context);
}

function view_2_column_boxes() {
  $context = _get_context();
  $context['white_box_content'] = get_field('white_box_content', 'option');
  $context['blue_box_content'] = get_field('blue_box_content', 'option');
  Timber::render('views/2-column-boxes.twig', $context);
}

function view_partners() {
  $context = _get_context();
  $context['partners_title'] = get_field('partners_title', 'option');
  $context['partners'] = get_field('partners', 'option');
  Timber::render('views/partners.twig', $context);
}

function view_sponsors() {
  $context = _get_context();
  $context['sponsors_title'] = get_field('sponsors_title', 'option');
  $context['sponsors'] = get_field('sponsors', 'option');
  Timber::render('views/sponsors.twig', $context);
}

function view_pre_footer() {
  $context = _get_context();
  $context['bottom_box_content'] = get_field('bottom_box_content', 'option');
  Timber::render('views/pre-footer.twig', $context);
}

function view_footer_items() {
  $context = _get_context();
  $context['footer_items'] = get_field('footer_items', 'option');
  Timber::render('views/footer-items.twig', $context);
}

function view_film_screenings() {
  $context = _get_context();
  $context['post'] = Timber::get_post();
  $context['films'] = array_map(
    function($film){
      $film->cf = get_fields($film->id);
      $film->location_times = array();
      if($film->cf['showtimes']) {
        foreach($film->cf['showtimes'] as $st){
          $dates = $st['dates'];
          foreach($st['times'] as $time) {
            if(is_object($time['location'])) {
              $post_title = $time['location']->post_title;
              $film->location_times[$dates][$post_title][] = $time['time'];
            }
          }
        }
      }
      return $film;
    },
    Timber::get_posts('post_type=film&numberposts=-1&orderby=title&order=ASC')
  );
  $context['event_dates'] = get_field('event_dates', 'option');
  $context['buttons'] = get_field('buttons');
  $context['genres'] = get_terms('film-genre');
  Timber::render('views/film-screenings.twig', $context);
}

function view_template($template) {
  $context = _get_context();
  Timber::render('views/'.$template.'.twig');
}

function view_page_content() {
  $context = _get_context();
  $context['page'] = Timber::get_post();
  $context['has_side_bar'] = false;
  $custom_fields = get_fields($context['page']->id);
  if(is_array($custom_fields['items'])) {
    $context['custom_fields'] = array_map(
      function($field){
        if($field['type'] == 'film-block') {
          $cf = get_fields($field['film']->ID);
          $field['cf'] = $cf;
        }
        return $field;
      },
      $custom_fields['items']
    );
  } else {
    $context['custom_fields'] = $custom_fields['items'];
  }
  if(is_array($context['custom_fields']) || count($custom_fields['sidebar_links']) > 0) {
    if($context['custom_fields']) {
      foreach($context['custom_fields'] as $field) {
        if($field['type'] == 'text-anchor') {
          $context['has_side_bar'] = true;
          break;
        }
      }
    }
  }
  $context['sidebar_links'] = $custom_fields['sidebar_links'];
  Timber::render('views/page-content.twig', $context);
}

function _get_context() {
  $timber = new \Timber\Timber();
  return Timber::get_context();
}
