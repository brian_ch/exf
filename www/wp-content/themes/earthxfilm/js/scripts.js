(function( $ ) {
  $('.hamburger').unbind('click').click(function(){
    $(this).parent().toggleClass('menu-open');
    if(!$(this).parent().hasClass('menu-open')) {
      $(this).parent().find('.item.open').removeClass('open');
      $(this).parent().find('.children').height(0);
    }
  });

  $(window).scroll(function () {
    if($(window).scrollTop() > ($('header').height() / 2)) $('header').addClass('background');
    else $('header').removeClass('background');
  });


    function calculateTime(dateFutureTS) {
      var dateFutureTS = dateFutureTS || 1492786800;
      var t = new Date(dateFutureTS * 1000) - new Date();
      var seconds = Math.floor( (t/1000) % 60 );
      var minutes = Math.floor( (t/1000/60) % 60 );
      var hours = Math.floor( (t/(1000*60*60)) % 24 );
      var days = Math.floor( t/(1000*60*60*24) );
      return {
        'total': t,
        'days': pad(days),
        'hours': pad(hours),
        'minutes': pad(minutes),
        'seconds': pad(seconds)
      };
    }
    function setTimes() {
      var time = calculateTime();
      $('#days').text(time.days);
      $('#hours').text(time.hours);
      $('#minutes').text(time.minutes);
      $('#seconds').text(time.seconds);
    }
    function pad(n) {
      return (n < 10) ? ("0" + n) : n;
    }

    function init() {
      setTimes();
      setInterval(function(){
        setTimes();
      }, 1000);
    }
    init();


    $('#modal').click(function(e){
      var $target = $(e.target);
      if($target.hasClass('button') || $target.is('#modal')) {
        $('body').removeClass('modal-active');
        $('#modal .content').removeClass('loaded');
      }
    });

    function getTicketHtml(post_id, cb) {
      var postData = {'action': 'add_foobar','post_id': post_id};
      $.post(
        MyAjax.ajaxurl,
        postData,
        function(response){
          var data = JSON.parse(response);
          cb(data);
        }
      );
    }

    function openModal(content) {
      $modContent = $('#modal .content');
      $modContent.html(content)
      $('body').addClass('modal-active');
      $('#modal iframe').load(function() {
        $modContent.addClass('loaded');
      });
    }

    if($('body').hasClass('page-template-default')) {
      $(document).ready(function(){
        $('.select-box .dropdown-box').click(function(){
          clickOutClose('.select-box');
          if(!$(this).parent().hasClass('open')) $('.select-box').removeClass('open');
          $(this).parent().toggleClass('open');
        });
        $('.select-box ul li').click(function(){
          $('.select-box.open').removeClass('open');
          var id = $(this).data('scroll-position');
          scrollToPos($(id), 50);
        });

        $('.box .share').click(function(){
          $(this).toggleClass('active');
        });
      });
    }

    if($('.get-ticket').length) {
      $(document).ready(function(){
        $('.get-ticket').click(function(){
          var id = $(this).data('id');
          getTicketHtml(id, function(data){
            openModal(data.widget_code);
          });
        });
      })
    }

    if($('body').hasClass('page-template-screenings')) {
      $(document).ready(function(){
        var curDate = currentDateString();
        var $curDateOption = $('li[data-value='+curDate+']');
        if($curDateOption.length) $curDateOption.click();
        else $('#date ul li:first-child').click();
        checkAllValues();
        setMinHeight();
      });

      function setMinHeight() {
        var theHeight = 0;
        var height;
        $('.select-box ul').each(function(){
           height = $(this).height();
          if(height > theHeight) theHeight = height;
        })
        var $filmsWrapper = $('.films-wrapper');
        var contentHeight = $filmsWrapper.height();
        console.log(height, contentHeight);
        if(height > contentHeight) {
          $filmsWrapper.css('min-height', height+'px')
        }
      }

      $('.films-list .film .expand').click(function(){
        $(this).closest('.film').toggleClass('expanded');
      });

      $('.films-list .film .description .share').click(function(){
        $(this).toggleClass('active');
      });

      $('.select-box .dropdown-box').click(function(e){
        clickOutClose('.select-box');
        if(!$(this).parent().hasClass('open')) $('.select-box').removeClass('open');
        $(this).parent().toggleClass('open');
        blurFilmsList();
      });

    }

    $('.item .has-dropdown').click(function(){
      var $parent = $(this).closest('.item');
      var $childrenParent = $parent.find('.children');
      $parent.toggleClass('open');
      var count = $parent.find('.children .child').length;
      if($parent.hasClass('open')) {
        $childrenParent.height(((count * 33) + 5)+'px');
      } else {
        $childrenParent.height(0);
      }
    });

    $('.select-box ul li').click(function(){
      var $selectBox = $(this).closest('.select-box');
      $selectBox.find('.dropdown-box').text($(this).text());
      $selectBox.removeClass('open');
      $selectBox.data('value', $(this).data('value'));

      $('.films-list .film').removeClass('expanded').find('.share').removeClass('active');

      checkAllValues();
      checkForVisibleFilms();
      setSearchCriteriaText();
      blurFilmsList();
      filterDates();
    });

    function clickOutClose(el) {
      if($(el).hasClass('open')) {
        $('body').click(function(e){
          var isChild = $(e.target).parents(el).length;
          if(!isChild) {
            $(el).removeClass('open');
            blurFilmsList();
          }
        });
      }
    }

    function filterDates() {
      var chosenDate = $('#date').data('value');
      $('.dates').hide();
      $('.date-'+chosenDate).show();
    }

    function blurFilmsList() {
      if($('.select-box').hasClass('open')){
        $('.films-wrapper').addClass('blur');
      } else {
        $('.films-wrapper').removeClass('blur');
      }
    }

    function setSearchCriteriaText() {
      var date = $('#date .dropdown-box').text();
      var genre = $('#genre .dropdown-box').text();
      $('#criteria-date').text(date);
      $('#criteria-genre').text(genre);
    }

    function checkAllValues() {
      var date = false;
      var genre = false;
      $('.select-box').each(function(){
        if($(this).is('#date')) date = $(this).data('value');
        else genre = $(this).data('value');
      });
      checkEachItem(date,genre);
    }

    function checkForVisibleFilms() {
      var count = $('.films-list .film:visible').length;
      if (count) $('.films-list .films-not-found').hide();
       else $('.films-list .films-not-found').fadeIn(200);
    }

    function checkEachItem(date, genre) {
      $('.films-list .film').each(function(){
        var genres = $(this).data('genres').toString();
        var dates = $(this).data('dates').toString();
        var show = false;
        if(date && genre) show = (genres.match(genre) && dates.match(date));
        else if(date && !genre) show = dates.match(date);
        else if(!date && genre) show = genres.match(genre);
        else show = true;
        if(!show) $(this).hide();
        else $(this).fadeIn();
      });
    }

    function currentDateString() {
      var utc = new Date().toJSON().slice(0,10).replace(/-/g,'');
      return utc;
    }

    function checkDate(date) {
      var dateToday = currentDateString();
      return dateToday == date;
    }

    if($('.sidebar').length) {
      var offsetAmount = 50;
      $('.sidebar .btn').click(function(){
        var id = $(this).attr('data-scroll-position');
        scrollToPos($(id), offsetAmount);
      });

      $(window).scroll(function(){
        var $anchorFirst = $('.anchor').first();
        var headerHeight = $('header').height() + offsetAmount;
        $('.anchor').each(function(){
          var top = $(this).offset().top - window.pageYOffset;
          if (top <= headerHeight) {
            var firstPosition = ($anchorFirst.offset().top - window.pageYOffset) - $anchorFirst.height();
            var $anchorButton = $('.sidebar .btn[data-scroll-position=#'+ $(this).attr('id') +']');
            $('.sidebar .btn').removeClass('active');
            $anchorButton.addClass('active');
          }
          if(($anchorFirst.offset().top - window.pageYOffset) > headerHeight) {
            $('.sidebar .btn').removeClass('active');
          }
        });
      });
    }

    function scrollToPos($el, offsetAmount) {
      $('html, body').animate({
          scrollTop: $el.offset().top - ($('header').height() + offsetAmount)
      }, 500);
    }

    if($('body').find('#slider').length > 0) {
      $(window).on('load resize', function(){
        var checkWidth = $(this).width() > 1250;
        var checkSlideShow = window.mySwipe == undefined;
        if(checkWidth && checkSlideShow) {
          initSlideShow();
        }
      });
    }


    function initSlideShow() {
      window.mySwipe = new Swipe(document.getElementById('slider'), {
        startSlide: 0,
        speed: 700,
        auto: 5000,
        continuous: true,
        disableScroll: false,
        stopPropagation: false,
        callback: function(index, elem) {
          $('.slider-controls li').removeClass('active');
          $('.slider-controls li[data-slide='+index+']').addClass('active');
        },
        transitionEnd: function(index, elem) {}
      });
      $('.slider-controls li').click(function(){
        window.mySwipe.slide($(this).data('slide'), 700);
      });
    }

})( jQuery );
