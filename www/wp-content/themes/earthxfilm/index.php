<?php
get_header();
view_header_menu();
?>
<main>
  <?php
    view_page_content();
  ?>
</main>
<?php
get_footer();
?>
