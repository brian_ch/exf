<?php
require_once(ABSPATH . 'vendor/autoload.php');
include('view_functions.php');
include('ajax_functions.php');

add_action( 'admin_menu', 'remove_menus' );
function remove_menus(){
  remove_menu_page( 'edit.php' );
}

add_action( 'init', 'create_custom_posttypes' );
function create_custom_posttypes() {

  register_post_type( 'homepage-card',
    array(
      'labels' => array(
        'name' => __( 'Homepage Cards' ),
        'singular_name' => __( 'Homepage Card' )
      ),
      'public' => true,
      'menu_icon' => 'dashicons-index-card',
      'has_archive' => true,
      'rewrite' => array( 'slug' => 'homepage-card' )
    )
  );

  register_post_type( 'location',
    array(
      'labels' => array(
        'name' => __( 'Locations' ),
        'singular_name' => __( 'Location' )
      ),
      'public' => true,
      'menu_icon' => 'dashicons-location',
      'has_archive' => true,
      'rewrite' => array( 'slug' => 'locations' )
    )
  );

  register_post_type( 'film',
    array(
      'labels' => array(
        'name' => __( 'Films' ),
        'singular_name' => __( 'Film' )
      ),
      'taxonomies' => array('genre'),
      'public' => true,
      'menu_icon' => 'dashicons-format-video',
      'has_archive' => true,
      'rewrite' => array( 'slug' => 'films' )
    )
  );

  $labels = array(
    'name' => _x( 'Genres', 'film genre name' ),
    'singular_name' => _x( 'Genre', 'film genre singular name' ),
    'search_items' =>  __( 'Search Genres' ),
    'popular_items' => __( 'Popular Genres' ),
    'all_items' => __( 'All Genres' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Genre' ),
    'update_item' => __( 'Update Genre' ),
    'add_new_item' => __( 'Add New Genre' ),
    'new_item_name' => __( 'New Genre Name' ),
    'separate_items_with_commas' => __( 'Separate genres with commas' ),
    'add_or_remove_items' => __( 'Add or remove genres' ),
    'choose_from_most_used' => __( 'Choose from the most used genres' ),
    'menu_name' => __( 'Genres' ),
  );


    register_taxonomy(
        'film-genre',
        'film',
        array(
            'labels' =>  $labels,
            'rewrite' => array( 'slug' => 'film-genre' ),
            'hierarchical' => true,
        )
    );
}

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Header Menu Settings',
		'menu_title'	=> 'Header Menu Settings',
		'menu_slug' 	=> 'header-menu-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

  acf_add_options_page(array(
    'page_title' 	=> 'Homepage Settings',
    'menu_title'	=> 'Homepage Settings',
    'menu_slug' 	=> 'homepage-settings',
    'capability'	=> 'edit_posts',
    'redirect'		=> false
  ));

  acf_add_options_page(array(
    'page_title' 	=> 'Site Settings',
    'menu_title'	=> 'Site Settings',
    'menu_slug' 	=> 'site-settings',
    'capability'	=> 'edit_posts',
    'redirect'		=> false
  ));

  acf_add_options_page(array(
    'page_title' 	=> 'Footer Items',
    'menu_title'	=> 'Footer Items',
    'menu_slug' 	=> 'footer-items',
    'capability'	=> 'edit_posts',
    'redirect'		=> false
  ));

}


add_action( 'wp_enqueue_scripts', 'earthxfilm_scripts' );
function earthxfilm_scripts() {
  wp_enqueue_style( 'styles', get_template_directory_uri() . '/style.css', array() );
  wp_enqueue_script( 'swipe', get_template_directory_uri() . '/js/swipe.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'earthxfilm_script', get_template_directory_uri() . '/js/scripts.js', array( 'jquery' ), null, true );
  wp_localize_script( 'earthxfilm_script', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}

function button_func($atts, $content) {
  $atts = shortcode_atts(
		array(
			'href' => '/',
			'target' => '_blank',
		), $atts, 'button' );
  return '<a href="'. $atts['href'] .'" target="'. $atts['target'] .'" class="btn">'.$content.'</a>';
}
add_shortcode( 'btn', 'button_func' );

function my_mce4_options($init) {
  $default_colours = '"FFFFFF", "White",
                      "25aae1", "Blue",
                      "000000", "Black"';
  $init['textcolor_map'] = '['.$default_colours.']';
  $init['textcolor_rows'] = 6;
  return $init;
}
add_filter('tiny_mce_before_init', 'my_mce4_options');
