<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

if($_SERVER['HTTP_HOST'] == 'coho-earthxfilm.herokuapp.com') {
	define('DB_NAME', 'heroku_93c96491fdd0377');
	define('DB_USER', 'b6054e32d45204');
	define('DB_PASSWORD', '3bd21897');
	define('DB_HOST', 'us-cdbr-iron-east-03.cleardb.net');
	define('WP_DEBUG', false);
} elseif($_SERVER['HTTP_HOST'] == 'beta.earthxfilm.org') {
	define('DB_NAME', 'earthxfi_db');
	define('DB_USER', 'earthxfi_user');
	define('DB_PASSWORD', 'rBwz%BEHar$G');
	define('DB_HOST', 'localhost');
	define('WP_DEBUG', false);
} else {
	define('DB_NAME', 'EXF_DB');
	define('DB_USER', 'EXF_User');
	define('DB_PASSWORD', '7qfbQG0g3GEI');
	define('DB_HOST', 'localhost');
	define('WP_DEBUG', true);
}
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_@!uQTB3:5y[?+j*@t??F1*U1E]&-Ac[/`J7x`t9;eIMzgvtGv@*VnM(70a?{(*#');
define('SECURE_AUTH_KEY',  ',5rz/Z7/LX2be2b_BR&wA{^zpw=JW|1)8EU?$I~?kMileO4K]n*1: XEx1A&:KS7');
define('LOGGED_IN_KEY',    'jri+et+=IlC.DVT{gIcp1oe{u>zesHzSB9~}*U.`W-]L8p#Y[OR?>Ef?G?2,(Yqf');
define('NONCE_KEY',        '|T5c!7KU5+xd:*!V4WLMS?`-:}kjz]vKA`mt-!fe/ZYK:5;=7A[<}(wJxa6:+>]p');
define('AUTH_SALT',        ']3P=.m44.,~B~K^BMvi;#w=vw|!~8b5>l.EZWHY>(X`?m!8VI*zC^wB,Rd#r(>_l');
define('SECURE_AUTH_SALT', 'Px5xk`z[uzS+<#/VFictFcOLX*-~(g/1%HAQ@_mvPh/V(dzHbv.~OlkA|y,t[`~h');
define('LOGGED_IN_SALT',   'lDmn&)[SG^$f!;iN9CQjQAw+nlL~P#TL=m0,:&$ZWH+/a7YB(|DXuAY!uCiO>O  ');
define('NONCE_SALT',       'dPo}JL o=Okl_~?HXI94CuOZ6OlLH|=j<b_gNHr=JI?Elvt:|S,Xn]+)C]S=}s,|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('AUTOSAVE_INTERVAL', 600);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
